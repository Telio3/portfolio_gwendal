# PortFolio Gwendal
***
Projet de Télio, étudiant en première année de développement.
***
***
L'idée est de créer un site afin de présenter l'ensemble des créations de l'artiste facilement. Et que ce dernier puisse ajouter librement ses dernières productions.

Ce site me permet, en tant qu'étudiant en première année, d'améliorer mes compétences en développement.
***
### Technologies
***
Liste des technologies utilisées :
* [PHP](https://www.php.net/): Version 7.4.9
* [SQL](https://sql.sh/): Version 5.7.31
***

![Work](https://media.giphy.com/media/xT8qBhrlNooHBYR9f2/giphy.gif)
